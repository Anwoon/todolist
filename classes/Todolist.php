<?php
class todoList
{
    private $title;
    private $id;

    function __construct(){
    }

    public function setId($value){
        $this->id = $value;
    }

    public function setTitle($value){
        $this->title = $value;
    }

    public function setContainer($value){
        $this->container = $value;
    }

    public function getId(){
        return $this->id;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getContainer(){
        return $this->container;
    }

    static public function addTodo ($todo) {
        array_push(self::$listTodo, $todo);
    }

    static public function getAllTodo () {
        return $listTodo;
    }

    public function addTodoList($title,$user,$password,$host,$database){
        $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
        $texte = $bd->prepare('INSERT INTO todolist(title) VALUES (:title)');
        $texte->execute(array(
            'title'=>$title
        ));
    }

    static public function updateTodoList($getText,$getListId,$user,$password,$host,$database){
        $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
        $idDel = $bd->prepare('UPDATE todolist SET title=:title WHERE id=:id');
        $idDel->execute(array(
            'id'=> $getListId,
            'title'=>$getText
        ));
    }

    static public function deleteTodoList($id,$user,$password,$host,$database){
    $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
    $texte = $bd->prepare('DELETE FROM todolist WHERE id=:id');
    $texte->execute(array(
        'id'=> $id
    ));
    $idDel = $bd->prepare('DELETE FROM todos WHERE todolistid=:id');
    $idDel->execute(array(
        'id'=> $id
    ));
    }
}


?>