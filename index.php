<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Bungee" rel="stylesheet">
    <?php include 'classes/Todo.php'?>
    <?php include 'classes/Todolist.php'?>
    <?php include 'classes/Db.php'?>
    <title>Todolist</title>
</head>

<body>
    <div id="createTodoList">
        <h4>Todo List</h4>
        <form action="./forms/addTodoList.php" method="POST" class="todoListForm">
            <input type="text" class="createList" name="createList" value="" placeholder="Name Todo List">
            <input type="submit" class="saveList" name="saveList" value="Créer">
        </form>
    </div>
    <br>
    <br>
    <div id="todolist">
        <?php
     $list = $bd->query('SELECT * FROM `todolist`');
     $todos = $bd->query('SELECT * FROM `todos`');

    $todos =  $todos->fetchAll();
    while($listes = $list->fetch())
    {
    ?>
            <div id="todo-list-all" />
            <form action="./forms/deleteTodoList.php" method="POST" class="deleteTodo">
                <input type="submit" class="deleteSubmit" name="deleteSubmit" value="X">
                <input id="listid" name="listid" type="hidden" value="<?php echo $listes['id'] ?>">
            </form>
            <br/>
            <form action="./forms/updateTodoList.php" method="GET" class="updateTodoList">
                <input type="text" class="titleTodoList" name="titleTodoList" value="<?php echo $listes['title']?>">
                <input id="listid" name="listid" type="hidden" value="<?php echo $listes['id'] ?>">
                <input type="image" class="updatelist" name="updateTodolist" src="assets/f3d48a1d.png">
            </form>
            <h2>
                <?php echo $listes['title']?>
            </h2>
            <br/>
            <?php
            foreach($todos as $todo){

                if ($listes['id'] === $todo['todolistid']) {
        ?>
                <div class="todoStyle">
                    <?php if($todo['done']==1){ echo '<input type="image" class="trueIMG" name="true" src="assets/fait.png">';}else{ echo '<input type="image" class="falseIMG" name="false" src="assets/pasfait.png">';} ?>
                    <p class="textTodo">
                        <?php echo $todo['text'];?>
                    </p>
                    <div class="buttonDeleteStyle">
                        <form action="./forms/deleteTodo.php" method="POST" class="deleteForms">
                            <input type="image" class="delete" name="delete" src="assets/delete.png">
                            <input id="deleteText" name="deleteText" type="hidden" value="<?php echo $todo['id']?>">
                        </form>
                    </div>
                </div>
                <div class="todo">
                    <form action="./forms/updateTodo.php" method="GET" class="todoForm">
                        <input type="checkbox" class="checkbox" name="checkbox" <?php if($todo['done']==1){echo "checked"; }?>>
                        <input type="text" class="echoTodo" name="echoTodo" value="<?php  echo $todo['text'];?>" placeholder=" Todo">
                        <input type="image" class="save" name="save" src="assets/f3d48a1d.png">
                        <input id="updateText" name="updateText" type="hidden" value="<?php echo $todo['id'] ?>">
                    </form>
                    <div class="buttonDelete">
                        <form action="./forms/deleteTodo.php" method="POST" class="deleteForms">
                            <input type="image" class="delete" name="delete" src="assets/delete.png">
                            <input id="deleteText" name="deleteText" type="hidden" value="<?php echo $todo['id']?>">
                        </form>
                    </div>
                </div>
                <?php
            }
        }
        ?>
                    <br/>
                    <br/>
                    <h3>Ajouter Todo:</h3>
                    <br/>
                    <div id="descriptions">
                        <form action="./forms/addTodo.php" method="POST" id="formDescription">
                            <input type="text" id="readInput" name="input" placeholder=" Votre todo ici" required>
                            <br/>
                            <input id="listid" name="listid" type="hidden" value="<?php echo $listes['id'] ?>">
                            <input type="submit" id="add" name="add">
                        </form>
                        <br/>
                    </div>
    </div>
    <?php
            }
        ?>
        </div>
</body>

</html>