<?php
class Todo
{
    private $text;
    private $done;
    private $id;

    function __construct($text,$done="false"){
        $this->setText($text);
        $this->setDone($done);
        $this->setId(uniqid());
    }

    public function setId($value){
        $this->id = $value;
    }

    public function setText($value){
        $this->text = $value;
    }

    public function setDone($value){
        $this->done = $value;
    }

    public function getText(){
        return $this->text;
    }

    public function getId(){
        return $this->id;
    }

    public function getDone(){
        return $this->done;
    }

    public function addTodo($text,$done,$listid,$user,$password,$host,$database){
        $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
        $texte = $bd->prepare('INSERT INTO todos(text, done, todolistid) VALUES (:text,:done,:todolistid)');
        $texte->execute(array(
            'text'=>$text,
            'done'=>$done,
            'todolistid'=>$listid
        ));
    }

    static public function deleteTodo($id,$user,$password,$host,$database){
        $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
        $idDel = $bd->prepare('DELETE FROM todos WHERE id=:id');
        $idDel->execute(array(
            'id'=> $id
        ));
    }

    static public function updateTodo($id,$text,$done,$user,$password,$host,$database){
        $bd = new PDO("mysql:host=$host;dbname=$database;charset=utf8",$user,$password);
        $idDel = $bd->prepare('UPDATE todos SET text=:text,done=:done WHERE id=:id');
        $idDel->execute(array(
            'id'=> $id,
            'text'=>$text,
            'done'=>$done
        ));
    }
}
?>