<?php
    include '../classes/Todolist.php';
    include 'config.php';

    $title = htmlspecialchars($_POST['createList']);
    $todolist = new todoList;
    $todolist->addTodoList($title,$user,$password,$host,$database);
    header('Location:'.$_SERVER['HTTP_REFERER']);
?>