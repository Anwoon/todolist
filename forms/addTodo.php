<?php
    include __DIR__.'/../classes/Todo.php';
    include 'config.php';

    $text = htmlspecialchars($_POST['input']);
    $done = 0;
    $listid = $_POST['listid'];
    //$list= $_POST['list'];
    $newTodo = new Todo($text, $done);
    $newTodo->addTodo($text,$done,$listid,$user,$password,$host,$database);

    header('Location:'.$_SERVER['HTTP_REFERER']);
?>